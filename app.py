import os
from flask import Flask, request
from flask_autoindex import AutoIndex

ppath = os.path.dirname(os.path.realpath(__file__))
app = Flask(__name__)
AutoIndex(app, browse_root=ppath, show_hidden=True)


@app.route("/secret", methods=["POST"])
def hello():
    # fetch password and compare
    with open("../password.txt", "r") as pfh, \
         open("../secret.txt", "r") as sfh:
        password = pfh.read().strip().split('=')[1]
        secret = sfh.read()
        if password == request.form.get('password', None):
            return secret
        return "Wrong password"
    return "Internal error"


if __name__ == "__main__":
    app.run()
